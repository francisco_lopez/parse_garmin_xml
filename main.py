import GarminTCXReader as Garmin
import mysql.connector
from mysql.connector import errorcode
from os import listdir

files = [f for f in listdir("C:/Users/Vincie/Dropbox/Tralkan/Relojes/data/new")]
dir = "C:/Users/Vincie/Dropbox/Tralkan/Relojes/data/new/"
filesUploaded = []

for file_tcx in files:
    filesUploaded.append(file_tcx)
    # file_tcx = "r88_20160115.tcx"
    parser = Garmin.GarminTCXReader(dir + file_tcx)
    codigo = file_tcx.split('_')[0]

    config = {
      'user': 'root',
      'password': '',
      'host': '127.0.0.1',
      'database': 'relojes',
      'raise_on_warnings': False,
      'use_pure': True,
    }

    create_tableTrack = ("""
    CREATE TABLE IF NOT EXISTS `track` (
      `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
      `codigo` varchar(10) NOT NULL,
      `time` datetime NOT NULL,
      `altitude` float NOT NULL,
      `distance` float NOT NULL,
      `heart_rate_bpm` tinyint(4) NOT NULL,
      `latitude` double NOT NULL,
      `longitude` double NOT NULL,
      PRIMARY KEY (`id`),
      UNIQUE KEY `codigo` (`codigo`,`time`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8
    """)

    create_tableLap = ("""
    CREATE TABLE IF NOT EXISTS `lap` (
      `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
      `codigo` varchar(10) NOT NULL,
      `idLap` datetime NOT NULL,
      `lapNr` int NOT NULL,
      `timeTot_s` float NOT NULL,
      `distance_m` float NOT NULL,
      `maxSpeed` float NOT NULL,
      `calories` double NOT NULL,
      `avgHeartRate_bpm` tinyint(4) NOT NULL,
      `maxHeartRate_bpm` double NOT NULL,
      PRIMARY KEY (`id`),
      UNIQUE KEY `codigoLap` (`codigo`, `idLap`, `lapNr`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8
    """)

    add_track = ("INSERT INTO track "
                 "(time, altitude, distance, heart_rate_bpm, latitude, longitude, codigo) "
                 "VALUES (%s, %s, %s, %s, %s, %s, %s)")
    add_lap = ("INSERT INTO lap "
               "(idLap, lapNr, timeTot_s, distance_m, maxSpeed, calories, avgHeartRate_bpm, maxHeartRate_bpm, codigo) "
               "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)")

    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()
    # cursor.execute(create_tableTrack, multi=True)
    result = parser.parse()
    resultLap = parser.parseLap()
    print result
    print resultLap

    for row in result:
        row.append(codigo)
        cursor.execute(add_track, row)
    for row in resultLap:
        row.append(codigo)
        cursor.execute(add_lap, row)
    cnx.commit()
    cnx.close()

print filesUploaded