import xml.etree.ElementTree as Et


class GarminTCXReader:
    """ Read and transform a Garmin track to a list """
    root = None
    ns = "{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}"

    def __init__(self, file_tcx):
        self.set_file(file_tcx)

    def set_file(self, file_tcx):
        tree = Et.parse(file_tcx)
        self.root = tree.getroot()

    def parse(self):
        ns = "{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}"
        list = []
        for activities in self.root:
            for activity in activities.iter(ns+"Activity"):
                id = activity.find(ns+"Id").text
                for track in activity.iter(ns+"Track"):
                    for trackpoint in track.iter(ns+"Trackpoint"):
                        node_altitude = trackpoint.find(ns+"AltitudeMeters")
                        if node_altitude is None:
                            altitude = 0
                        else:
                            altitude = node_altitude.text
                        node_time = trackpoint.find(ns+"Time")
                        if node_time is None:
                            time = ""
                        else:
                            time = node_time.text
                        node_distance = trackpoint.find(ns+"DistanceMeters")
                        if node_distance is None:
                            distance = 0
                        else:
                            distance = node_distance.text

                        heart_rate_bpm = 0
                        node_heart_rate_bpm = trackpoint.find(ns+"HeartRateBpm")
                        if node_heart_rate_bpm is not None:
                            node_value = node_heart_rate_bpm.find(ns+"Value")
                            if node_value is not None:
                                heart_rate_bpm = node_value.text

                        latitude = 0
                        longitude = 0
                        node_position = trackpoint.find(ns+"Position")
                        if node_position is not None:
                            node_lat = node_position.find(ns+"LatitudeDegrees")
                            node_long = node_position.find(ns+"LongitudeDegrees")
                            if node_lat is not None:
                                latitude = node_lat.text
                            if node_long is not None:
                                longitude = node_long.text
                        list.append([time, altitude, distance, heart_rate_bpm, latitude, longitude])
        return list

    def parseLap(self):
        ns = "{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}"
        listLap = []
        lapNr = 0

        for laps in self.root[0][0]:
            for id in laps.iter(ns + "Id"):
                ID = id.text
            for lap in laps.iter(ns + "Lap"):
                lapNr = lapNr + 1
                timeTot = lap.find(ns + "TotalTimeSeconds").text
                distM = lap.find(ns + "DistanceMeters").text
                maxSpeed = lap.find(ns + "MaximumSpeed").text
                cal = lap.find(ns + "Calories").text
                avgHeartRate_node = lap.find(ns + "AverageHeartRateBpm")
                if avgHeartRate_node is None:
                    avgHeartRate = 0
                else:
                    avgHeartRate = avgHeartRate_node[0].text
                maxHeartRate_node = lap.find(ns + "MaximumHeartRateBpm")
                if maxHeartRate_node is None:
                    maxHeartRate = 0
                else:
                    maxHeartRate = maxHeartRate_node[0].text
                listLap.append([ID, lapNr, timeTot, distM, maxSpeed, cal, avgHeartRate, maxHeartRate])

        return listLap

